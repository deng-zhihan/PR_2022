from sklearn.datasets import load_iris
import math

class KNN():

    def __init__(self):
        pass

    def distance(self,train,test):
        """

        :param test:
        :param train:
        :return: distance between test and train

        """

        sum = 0
        for i in range(len(test)):
            sum += math.pow(test[i] - train[i],2)
        result = math.sqrt(sum)

        return result

    def classify(self,k,train,test,label):
        """

        :param train:
        :param test:
        :param label:
        :return:
        """

        result = []
        for i in range(len(test)):
            tempdistance = {}
            for j in range(len(train)):
                tempdistance.update({j:self.distance(train[j],test[i])})
            distance = sorted(tempdistance.items(), key=lambda d: d[1])
            result.append(self.search(k,label,distance))

        return result

    def search(self,k,label,distance):
        """

        :param k: choice k label
        :param label: data label
        :param distance: a list.
        :return:label
        """

        addresslabel = []
        for i in range(k):
            addresslabel.append(label[distance[i][0]])

        templabel = list(set(addresslabel))
        labelcountresult = {}
        for label in templabel:
            count = 0
            for address in addresslabel:
                if label == address:
                    count += 1
            labelcountresult.update({label:count})
	# labelcountresult={}
	# for lab in addresslabel:
	#     if lab in labelcountresult:
	#         labelcountresult[lab] += 1
	#     else:
	#         labelcountresult[lab] = 1

        result = sorted(labelcountresult.items(), key=lambda d: d[1],reverse=True)[0][0]

        return result

if __name__ == "__main__":

    import numpy as np

#   traindata = np.array([[1.0, 1.1],[1.0,1.0],[0,0],[0,0.1]])
#   labels = ['A', 'A', 'B', 'B']
#   testdata = [[1,0.9],[0,0.3]]

    obj = KNN()
#   result = obj.classify(3,traindata,testdata,labels)
#   print(result)

    iris = load_iris()
    irisdata = iris.data
    irislabel = iris.target
    test = [[5.5,2.3,4,1.3]]

    import time
    start = time.time()

    result = obj.classify(10,irisdata,test,irislabel)

    end = time.time()
    print(end -start)
    print(result)
