clear all;clc;
warning('off');
%读取数据
load fisheriris
X = meas(:,3:4);
Y = species;
ratio=0.1;%测试数据所占比例
[N,M]=size(X);
Y_species=zeros(N,1);
for species_index=1:N
    if strcmp(Y{species_index,1},'setosa')
        Y_species(species_index,1)=1;
    elseif strcmp(Y{species_index,1},'versicolor')
        Y_species(species_index,1)=2;
    else
        Y_species(species_index,1)=3;
    end
end
K=3;
trainData=X;
trainClass=Y;
num_test=N*ratio;
%归一化处理newData=(oldData-minValue)/(maxValue-minValue);
minValue=min(trainData);
maxValue=max(trainData);
trainData=(trainData-repmat(minValue,N,1))./(repmat(maxValue-minValue,N,1));
%计算出每一类的平均值
X_mean=[mean(X(find(Y_species==1),1:2));mean(X(find(Y_species==2),1:2));...
    mean(X(find(Y_species==3),1:2))];
%画出由knn算法得出的区域图
x_plot=(min(X(:,1))):0.05:(max(X(:,1)));
y_plot=(min(X(:,2))):0.05:(max(X(:,2)));
for x_plot_index=1:length(x_plot)
    for y_plot_index=1:length(y_plot)
        distence=pdist2([x_plot(x_plot_index),y_plot(y_plot_index)],X_mean(:,1:2));
        plot_ans(x_plot_index,y_plot_index)=find(distence==min(distence));
    end
end
for plot_index=1:3
    [x,y]=find(plot_ans==plot_index);
    plot(x_plot(x),y_plot(y),'.');
    hold on;
end
for plot_index=1:3
    Data=X(find(Y_species==plot_index),1:2);
    plot(Data(:,1),Data(:,2),'*');
    hold on;
end
plot(4,1.3,'o');
legend('Pre-set','Pre-ver','Pre-vir','Data-set','Data-ver','Data-vir','Predict');













