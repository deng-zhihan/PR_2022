clear all;clc;close all;
warning('off');
%% 读取数据
load fisheriris
X = meas(:,1:4);
Y = species;
[N,M]=size(X);
Y_species=zeros(N,1);
for species_index=1:N
    if strcmp(Y{species_index,1},'setosa')
        Y_species(species_index,1)=1;
    elseif strcmp(Y{species_index,1},'versicolor')
        Y_species(species_index,1)=2;
    else
        Y_species(species_index,1)=3;
    end
end
Data=[X,Y_species];
%随机打乱数据
rowrank = randperm(size(Data, 1));
Data_rand = Data(rowrank,:); 
ratio=0.3;%测试数据所占比例
ratio_num=round(length(Data)*ratio);
%% 得到训练集与测试集
%训练集
Train_Data = Data_rand(1:ratio_num,1:4);
Train_Label = Data_rand(1:ratio_num,5);
%测试集
Test_Data = Data_rand(ratio_num+1:150,1:4);
Test_Label = Data_rand(ratio_num+1:150,5);
%% 使用决策树进行训练及预测
Tree = ClassificationTree.fit(Train_Data,Train_Label);
view(Tree);
view(Tree,'mode','graph');
%预测分类
Tree_pre = predict(Tree,Test_Data);
%计算准确率
right_num=0;
for pre_index=1:length(Tree_pre)
    if Test_Label(pre_index)==Tree_pre(pre_index)
        right_num=right_num+1;
    end
end
right = right_num/length(Tree_pre);
disp(['模型准确率为:',num2str(right)]);













