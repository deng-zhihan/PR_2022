% least square regression using regress and regstats

clear all; clc;

% ---------------------------------------------------------------------
% example 1
% % source: https://www.mathworks.com/help/stats/regress.html
% 
% % https://www.mathworks.com/help/stats/sample-data-sets.html
% % Load the carsmall data set. Identify weight and horsepower as predictors and mileage as the response.
% load carsmall
% x1 = Weight; % Weight contains the weight of each car.
% x2 = Horsepower;    
% y = MPG; % variable MPG contains measurements on the miles per gallon of 100 sample cars. 
% 
% % Compute the regression coefficients for a linear model with an interaction term.
% X = [ones(size(x1)) x1 x2 x1.*x2];
% b = regress(y,X)    
% 
% % Plot the data and the model.
% scatter3(x1,x2,y,'filled')
% hold on
% x1fit = min(x1):100:max(x1);
% x2fit = min(x2):10:max(x2);
% [X1FIT,X2FIT] = meshgrid(x1fit,x2fit);
% YFIT = b(1) + b(2)*X1FIT + b(3)*X2FIT + b(4)*X1FIT.*X2FIT;
% mesh(X1FIT,X2FIT,YFIT)
% xlabel('Weight')
% ylabel('Horsepower')
% zlabel('MPG')
% view(50,10)
% hold off

% ---------------------------------------------------------------------
% example 2
X = [-5, -5; -5, -4; -4, -5; -5 -6; -6 -5; 5 5; 5 4; 4 5; 5 6; 6 5]; 
label = [1 1 1 1 1 -1 -1 -1 -1 -1];
[N, ~] = size(X);
Z = [ones(N, 1), X];
a = regress(label', Z);
y_x1 = [-6:0.01:6];
y_x2 = [-6:0.01:6];
for index1=1:length(y_x1)
    for index2=1:length(y_x2)
        y(index1,index2) = a(1)+y_x1(index1).*a(2)+y_x2(index2).*a(3);
    end
end
scatter3(Z(:,1),Z(:,2),Z(:,3),'filled');
hold on;
mesh(y_x1,y_x2,y);
% regstats(label', X)


