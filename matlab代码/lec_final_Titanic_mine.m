%% 加载数据集
clc;clear;close all;
load train.mat;
load test.mat;
Train_data = train(:,[1,2,3,5,6,7,8,10,12]);
Test_data = test(:,[2,4,5,6,7,8,9,11]);
num=0;
%% 对训练集进行处理
%将字符串转换为数值
for Train_data_index=1:length(Train_data)
    if strcmp(Train_data{Train_data_index,4},'male')
        Train_data(Train_data_index,4)={1};
    else
        Train_data(Train_data_index,4)={0};
    end
    if strcmp(Train_data{Train_data_index,9},'S')
        Train_data(Train_data_index,9)={1};
    elseif strcmp(Train_data{Train_data_index,9},'C')
        Train_data(Train_data_index,9)={2};
    else
        Train_data(Train_data_index,9)={3};
    end
end
%去除年龄未知的数据
delet_cell=[];
for Train_data_index=1:length(Train_data)
    if isnan(Train_data{Train_data_index,5})
        delet_cell=[delet_cell;Train_data_index];
    end
end
Train_data(delet_cell,:)=[];
Train_data_real=cell2mat(Train_data(:,2:9));
Train_data=cell2mat(Train_data(:,2:9));
%% 对测试集进行处理
%将字符串转换为数值
for Test_data_index=1:length(Test_data)
    if strcmp(Test_data{Test_data_index,2},'male')
        Test_data(Test_data_index,2)={1};
    else
        Test_data(Test_data_index,2)={0};
    end
    if strcmp(Test_data{Test_data_index,8},'S')
        Test_data(Test_data_index,8)={1};
    elseif strcmp(Test_data{Test_data_index,8},'C')
        Test_data(Test_data_index,8)={2};
    else
        Test_data(Test_data_index,8)={3};
    end
end
Test_data=cell2mat(Test_data);
%% 对数据进行归一化
%对训练集进行处理
[row,col]=size(Train_data);
Train_data=Train_data';
for Train_data_index=1:col
     Train_data(Train_data_index,:)=mapminmax(Train_data(Train_data_index,:));
end
%增加年龄小的乘客的权值
Train_data(4,:)=abs(Train_data(4,:));
Train_data(4,:)=mapminmax(Train_data(4,:));
Train_data=Train_data';

%对测试集进行处理
[row,col]=size(Test_data);
Test_data=Test_data';
for Test_data_index=1:col
     Test_data(Test_data_index,:)=mapminmax(Test_data(Test_data_index,:));
end
%增加年龄小的乘客的权值
Test_data(3,:)=abs(Test_data(3,:));
Test_data(3,:)=mapminmax(Test_data(3,:));
Test_data=Test_data';
%% 分析相关性，对特征进行选择
Pearson=corr(Train_data,'type','Pearson');
Spearman=corr(Train_data,'type','Spearman');
%画Pearson的相关性的热力图
xvalues = {'Survived','Pclass','Sex','Age','SibSp','Parch','Fare','Embarked'}; 
yvalues = {'Survived','Pclass','Sex','Age','SibSp','Parch','Fare','Embarked'}; 
yvalues=yvalues';
h1=HeatMap(Pearson,'Colormap',redbluecmap,'ColumnLabels',yvalues,'RowLabels',xvalues); 
h1.Annotate = true;%在热图中显示数据值
h2=HeatMap(Spearman,'Colormap',redbluecmap,'ColumnLabels',yvalues,'RowLabels',xvalues); 
h2.Annotate = true;%在热图中显示数据值

%% 用决策树进行训练和预测
%随机森林训练
Mdl = TreeBagger(200,Train_data(:,[2,3,7]),Train_data(:,1),'OOBPrediction','On',...
    'Method','classification');
view(Mdl.Trees{100},'Mode','graph');
view(Mdl.Trees{40},'Mode','graph');
view(Mdl.Trees{70},'Mode','graph');
oobErrorBaggedEnsemble = oobError(Mdl);
figure;
plot(oobErrorBaggedEnsemble);
xlabel 'Number of grown trees';
ylabel 'Out-of-bag classification error';
%用1-200棵树进行预测
pre=[];
RF_pre_result=[];
for tree_index=40:100
    pre = [pre,predict(Mdl.Trees{tree_index},Test_data(:,[1,2,7]))];
end
[pre_row,pre_col]=size(pre);
for pre_row_index=1:pre_row
    for pre_col_index=1:pre_col
        RF_pre_result(pre_row_index,pre_col_index)=str2double(pre{pre_row_index,pre_col_index});
    end
end
RF_pre_result=sum(RF_pre_result,2);
RF_pre_result(find(RF_pre_result<0))=0;
RF_pre_result(find(RF_pre_result>0))=1;
%计算准确率
load gendersubmission.mat;
right_num_RT=0;
for gender_index=1:length(gendersubmission)
    if RF_pre_result(gender_index)==gendersubmission(gender_index)
        right_num_RT=right_num_RT+1;
    end
end
disp(['随机森林模型准确率为：',num2str(right_num_RT/length(gendersubmission))]);

%% 用SVM模型训练和预测模型
SVMModel = fitcsvm(Train_data(:,[2,3,7]),Train_data(:,1),'ClassNames',[false true],'Standardize',true,...
        'KernelFunction','rbf','BoxConstraint',1);
[~,score] = predict(SVMModel,Test_data(:,[1,2,7]));
svm_pre_result=[];
for score_index=1:length(score)
    if score(score_index,1)>0
        svm_pre_result=[svm_pre_result;1];
    else
        svm_pre_result=[svm_pre_result;0];
    end
end
right_num_SVM=0;
for svm_pre_index=1:length(svm_pre_result)
    if svm_pre_result(svm_pre_index)==gendersubmission(svm_pre_index)
        right_num_SVM=right_num_SVM+1;
    end
end
disp(['SVM模型准确率为：',num2str(right_num_SVM/length(gendersubmission))]);
Train_data_plt=[Train_data_real(:,1),Train_data_real(:,[4,7])];
Train_data_S=[];Train_data_D=[];
%画出SVM分类图
for Train_data_plt_index=1:length(Train_data_plt)
    if Train_data_plt(Train_data_plt_index,1)==1
        Train_data_S=[Train_data_S;Train_data_plt(Train_data_plt_index,:)];
    else
        Train_data_D=[Train_data_D;Train_data_plt(Train_data_plt_index,:)];
    end
end
figure;
plot(Train_data_S(:,2),Train_data_S(:,3),'*');
hold on;
plot(Train_data_D(:,2),Train_data_D(:,3),'*');
xlabel('Age');
ylabel('Fare');
hold off;

%% 用KNN模型训练和预测
Train_data_S=[];Train_data_D=[];
for Train_data_plt_index=1:length(Train_data_plt)
    if Train_data_plt(Train_data_plt_index,1)==1
        Train_data_S=[Train_data_S;Train_data(Train_data_plt_index,:)];
    else
        Train_data_D=[Train_data_D;Train_data(Train_data_plt_index,:)];
    end
end
Train_data_S_mean=mean(Train_data_S(:,[2,3,7]));
Train_data_D_mean=mean(Train_data_D(:,[2,3,7]));
knn_pre_result=[];
S_point=[];D_point=[];
for Test_data_index=1:length(Test_data)
    S=pdist2(Test_data(Test_data_index,[1,2,7]),Train_data_S_mean);
    D=pdist2(Test_data(Test_data_index,[1,2,7]),Train_data_D_mean);
    if S<D
        knn_pre_result=[knn_pre_result;1];
        S_point=[S_point;S];
    else
        knn_pre_result=[knn_pre_result;0];
        D_point=[D_point;S];
    end
end
right_num_knn=0;
for knn_pre_index=1:length(knn_pre_result)
    if knn_pre_result(knn_pre_index)==gendersubmission(knn_pre_index)
        right_num_knn=right_num_knn+1;
    end
end
disp(['KNN模型准确率为：',num2str(right_num_knn/length(gendersubmission))]);
% 画图
S_zeros=zeros(length(S_point),1);
D_zeros=zeros(length(D_point),1);
figure;
plot(S_point,S_zeros,'o');
hold on;
plot(D_point,D_zeros,'o');
xlabel('Point');
legend('Survive','Passed');
hold off;
figure;
[X,Y,Z]=sphere(128);
r=0.8;
mesh(r*X+Train_data_D_mean(1),r*Y+Train_data_D_mean(2),r*Z+1+Train_data_D_mean(3));
hold on;
mesh(r*X+Train_data_S_mean(1),r*Y+Train_data_S_mean(2),r*Z+1+Train_data_S_mean(3));
hold on;
plot3(Train_data_D_mean(1),Train_data_D_mean(2),Train_data_D_mean(3));
hold off;
xlabel('Sex');ylabel('Pclass');zlabel('Fare');

