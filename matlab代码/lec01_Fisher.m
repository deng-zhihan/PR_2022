% % Fisher (linear) discriminant analysis
% Input:
%   X: d x n data matrix
%   label: 1 x n class label


clear all; clc;

X = [-5 -5; -5 -4; -4, -5; -5 -6; -6 -5; 5 5; 5 4; 4 5; 5 6; 6 5];
label = [1 1 1 1 1 -1 -1 -1 -1 -1];

index1 = find(label == 1);
index2 = find(label == -1);

% 两类样本数
N1 = length(index1);
N2 = length(index2);

% 计算两类均值
mu1 = mean(X(index1, :));
mu2 = mean(X(index2, :));

% 计算类内离散度矩阵
S1 = (X(index1, :)-mu1)'*(X(index1, :)-mu1);
S2 = (X(index2, :)-mu2)'*(X(index2, :)-mu2);

Sw = S1 + S2;  % 计算总类内离散度矩阵

W = Sw\(mu1 - mu2)';  % 求投影方向

z1 = W'*mu1';   z2 = W'*mu2'; % 求一维样本均值
z0 = (z1 + z2)/2;  % 求阈值点

figure, plot(X(index1, 1), X(index1, 2), 'ro', X(index2, 1), X(index2, 2), 'b*');

x1 =-6:0.1:6;
x2 = -(W(1)*x1-z0)/W(2); % 计算分界面上的点
hold on;
plot(x1, x2, 'g--'); % 绘制分界面

x = [3, 1]'; 
plot(x(1), x(2), 'rp'); % 绘制待测样本点

z = W'*x; % 待测样本降维

if z>z0
    result = '属于第一类';
else
    result = '属于第二类';
end

result = strcat('(', num2str(x'), ')', result);
title('Fisher线性判别');
line1 = 'x1'; line2 = result;
xlabel({line1; line2});
ylabel('x2');
hold off;





