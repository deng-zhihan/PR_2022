%% 加载数据并训练模型
clc;clear;close all;
load fisheriris;
 C2 = cvpartition(species,'KFold', 10);
rng(1); % For reproducibility
Mdl = TreeBagger(50,meas,species,'OOBPrediction','On',...
    'Method','classification');
view(Mdl.Trees{50},'Mode','graph');
figure;
oobErrorBaggedEnsemble = oobError(Mdl);
plot(oobErrorBaggedEnsemble);
xlabel 'Number of grown trees';
ylabel 'Out-of-bag classification error';
%% 利用第30-50个树预测数据[5.7 2.6 3.5 1]
predict_data=[5.7 2.6 3.5 1];
setosa=0;
versicolor=0;
virginica=0;
for tree_index=1:50
    pre = predict(Mdl.Trees{tree_index},predict_data);
    switch pre{1}
        case 'setosa'
            setosa=setosa+1;
        case 'versicolor'
            versicolor=versicolor+1;
        case 'virginica'
            virginica=virginica+1;
    end
end
predata = [setosa,versicolor,virginica];
n = find(predata==max(predata));
switch n
    case 1
        disp(['预测结果为setosa,confidence:',num2str(setosa/50)]);
    case 2
        disp(['预测结果为versicolor,confidence:',num2str(versicolor/50)]);
    case 3
        disp(['预测结果为virginica,confidence:',num2str(virginica/50)]);
end
        
        
        
        